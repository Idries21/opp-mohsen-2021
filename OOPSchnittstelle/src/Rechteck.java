
public class Rechteck extends GeoObject implements Geo {

	private double hoehe;
	private double breite;

	public Rechteck(double x, double y, double hoehe, double breite) {
		super(x, y);
		this.hoehe = hoehe;
		this.breite = breite;
	}

	@Override
	public double determineArea() {
		return hoehe * breite;

	}

	@Override
	public String toString() {
		return super.toString()+"Rechteck [hoehe=" + hoehe + ", breite=" + breite + " ,Flaeche:"+determineArea()+"]";
	}

}
