
public interface Geo {

	double PI = 3.1415;

	double determineArea();

}
