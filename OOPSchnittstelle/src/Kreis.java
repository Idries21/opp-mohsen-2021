public class Kreis extends  GeoObject implements Geo{
	private double radius;

	public Kreis(double x, double y, double radius) {
		super(x,y);
		this.radius = radius;
	}

	public double determineArea() {
		return radius*radius *PI;

	}

	@Override
	public String toString() {
		return super.toString()+"Kreis [radius=" + radius + " ,Flaeche:"+determineArea()+ "]";
	}

}
