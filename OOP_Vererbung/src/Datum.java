
public class Datum {
	private int Tag;
	private int Monat;
	private int Jahr;
	

	public Datum() { // Standardkonstruktor
		this.Tag = 24;
		this.Monat = 12;
		this.Jahr = 2014;
		
	}

	public Datum(int Tag, int Monat, int Jahr) { // Konstruktor, der alle Attribute mit Werten versieht.
		this.Tag = Tag;
		this.Monat = Monat;
		this.Jahr = Jahr;

	}

	public void setTag(int Tag) {
		this.Tag = Tag;
	}

	public int getTag() {
		return this.Tag;
	}

	public void setMonat(int Monat) {
		this.Monat = Monat;
	}

	public int getMonat() {
		return this.Monat;
	}

	public void setJahr(int Jahr) {
		this.Jahr = Jahr;
	}

	public int getJahr() {
		return this.Jahr;
	}

	// toString()
	public String toString() {
		return  this.Tag + "." + this.Monat + "." + this.Jahr;
	}

	

}
