import java.util.Scanner;
public class BankkontoTest {
	public static void main(String[] args) {
		double betrag;
		
		Bankkonto p1 = new Bankkonto("Sam", 1234, 60);
		System.out.println(p1);
		p1.einzahlen();
		Scanner scanner = new Scanner(System.in);
		System.out.println("wie viel m�chten Sie auszahlen ");
		betrag = scanner.nextDouble();
		p1.auszahlen(betrag);
		System.out.println(p1);
		System.out.println("=======================================================================================");
		Dispokonto f1 = new Dispokonto("Samer", 56789, 60, 100);
		System.out.println(f1);
		f1.einzahlen();
		System.out.println("wie viel m�chten Sie auszahlen ");
		betrag = scanner.nextDouble();
		f1.auszahlen(betrag);
		System.out.println(f1);
		System.out.println("========================================================================================");
	
		Bankkonto[] bliste = new Bankkonto[4]; //soll von Typ oberklasse sein 
		bliste [0]=new Bankkonto("Sam",1234,500);
		bliste [0]=new Bankkonto("Sam",1234,60);
		bliste [1]=new Bankkonto("Jan",5678,90);
		bliste[2]= new Dispokonto("Ali",1304,50,100);
		bliste[3]= new Dispokonto("Slah",8745,70,100);
		
		System.out.println(bliste [0]);
		System.out.println(bliste [1]);
		System.out.println(bliste [2]);
		System.out.println(bliste [3]);

	}
}
