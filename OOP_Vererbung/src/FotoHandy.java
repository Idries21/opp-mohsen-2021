
public class FotoHandy extends Handy {
	private int pixel;
	private String firma;
	public static String klassenName = "FotoHandy";

	public FotoHandy(String firma1, String typ, int pixel, String firma2) {
		super(firma1, typ);
		this.pixel = pixel;
		this.firma = firma2;
	}

	public int getPixel() {
		return pixel;
	}

	public void setPixel(int pixel) {
		this.pixel = pixel;
	}

	public String getFirma() {
		return firma;
	}

	public void setFirma(String firma) {
		this.firma = firma;
	}

	public String toString() {
		return "FotoHandy [firma(super)=" + super.getFirma() + ", typ=" + getTyp() + ", pixel=" + pixel + ", firma="
				+ firma + "]";
	}

	
	public static void main(String[] args) {
		Handy h = new Handy("Nokia ", "C62");
		System.out.println("h: " + h);
		FotoHandy f = new FotoHandy("Apple", "TS8", 5000000, "Hawawi");
		System.out.println("f: " + f);
		System.out.println("f.super: " + ((Handy) f).toString());
		System.out.println("f.klassenName: " + f.klassenName);
		System.out.println("f.klassenName: " + ((Handy) f).klassenName);
		System.out.println("f.super: " + ((Handy) h).toString());
		
	}
}

// Die Ausgabe lautet: h: Handy [firma=Nokia1, typ=C62]
/*
f: FotoHandy [firma(super)=Nokia1, typ=TS8, pixel=5000000, firma=Nokia2]
f.super: FotoHandy [firma(super)=Nokia1, typ=TS8, pixel=5000000, firma=Nokia2]
f.klassenName: FotoHandy
f.klassenName: Handy
*/