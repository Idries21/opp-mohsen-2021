
public class Feiertag extends Datum {
	protected String feiertagname;

	public Feiertag(int Tag, int Monat, int Jahr, String feiertagname) {
		super(Tag, Monat, Jahr);
		setFeiertagname(feiertagname);
	}

	public Feiertag() {
		super();
		this.feiertagname = "Heilig Abend";

	}

	public String getFeiertagname() {
		return feiertagname;
	}

	public void setFeiertagname(String feiertagname) {
		this.feiertagname = feiertagname;
	}

	@Override
	public String toString() {
		return  super.toString() + "  ("+ feiertagname + ").";
	}

}
