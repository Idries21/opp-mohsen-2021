import java.util.Scanner;

public class Bankkonto {
	private String inhaber;
	private int kontonummer;
	private double kontostand;

	public Bankkonto(String inhaber, int kontonummer, double kontostand) {
		this.inhaber = inhaber;
		this.kontonummer = kontonummer;
		this.kontostand = kontostand;

	}

	public Bankkonto() {
		this.inhaber = "Adam";
		this.kontonummer = 12345;
		this.kontostand = 20049.89;
	}

	public String getInhaber() {
		return inhaber;
	}

	public void setInhaber(String inhaber) {
		this.inhaber = inhaber;
	}

	public int getKontonummer() {
		return kontonummer;
	}

	public void setKontonummer(int kontonummer) {
		this.kontonummer = kontonummer;
	}

	public double getKontostand() {
		return kontostand;
	}

	public void setKontostand(double kontostand) {
		this.kontostand = kontostand;
	}

	public double einzahlen() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("wie viel m�chten Sie einzahlen ");
		double einzahlen = scanner.nextDouble();
		this.kontostand += einzahlen;
		return einzahlen;
	}

	public double auszahlen(double betrag) {
		setKontostand(this.kontostand - betrag);
		System.out.println("Sie haben " + betrag + " �" + " ausgezahlt");
		System.out.println("Aktuelle Kontostand ist:  " + getKontostand()+" �");
		return betrag;
	}

	@Override
	public String toString() {
		return "Bankkonto [inhaber=" + inhaber + ", kontonummer=" + kontonummer + ", kontostand=" + kontostand + " �]";
	}

}
