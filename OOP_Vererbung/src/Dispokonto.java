import java.util.Scanner;

public class Dispokonto extends Bankkonto {
	private double dispokredit;

	public Dispokonto(String inhaber, int kontonummer, double kontostand, double dispokredit) {
		super(inhaber, kontonummer, kontostand);
		setDispokredit(dispokredit);

	}

	public Dispokonto() {
		super();
		this.dispokredit = 100;

	}

	public double getDispokredit() {
		return dispokredit;
	}

	public void setDispokredit(double dispokredit) {
		this.dispokredit = dispokredit;
	}

	@Override
	public double auszahlen(double betrag) {
		if (betrag > getKontostand()) {
			if ((this.getKontostand() + this.dispokredit) >= betrag) {
				setKontostand(this.getKontostand() - betrag);
				System.out.println("Sie k�nnten " + betrag + " �" + " auszahlen");
				System.out.println("Aktuelle Kontostand ist:  " + getKontostand()+" �");
			} else {
				betrag =(getKontostand() + dispokredit);
				setKontostand(this.getKontostand() - this.dispokredit);
				System.out.println("Sie k�nnten nur " + betrag + " �" + " auszahlen");
				System.out.println("Aktuelle Kontostand ist:  " + -dispokredit+" �");
			}
		}

		else {
			setKontostand(this.getKontostand() - betrag);
			System.out.println("Sie k�nnten" + betrag + " �" + " auszahlen");
			System.out.println("Aktuelle Kontostand ist:  " + getKontostand()+" �");
		}
		return betrag;
	}

	@Override
	public String toString() {
		return super.toString() + " ,Dispokredit:" + dispokredit + "]";
	}

}
