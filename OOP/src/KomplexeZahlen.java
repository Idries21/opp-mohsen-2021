
public class KomplexeZahlen {
	private double Real;
	private double Imaginaer;
	private static double multi; // Atrribute Einer Klassenmethode (Zusatz static) zur Multiplikation zweier
									// komplexer Zahlen
	private double multi1; // Atrribute einer Objektmethode zur Multiplikation zweier komplexer Zahlen

	public KomplexeZahlen() { // Standardkonstruktor
		this.Real = 0.0;
		this.Imaginaer = 0.0;

	}

	public KomplexeZahlen(double Real, double Imaginaer) { // Konstruktor der alle Attribute mit Werten versieht
		this.Real = Real;
		this.Imaginaer = Imaginaer;

	}

	public void setReal(double Real) {
		this.Real = Real;
	}

	public double getReal() {
		return this.Real;
	}

	public void setImaginaer(double Imaginaer) {
		this.Imaginaer = Imaginaer;
	}

	public double getImaginaer() {
		return this.Imaginaer;
	}

	// toString()
	public String toString() {
		return "Komplexzahl:(" + this.Real + "," + this.Imaginaer + ")";
	}

	// equals(Object obj)
	public boolean equals(Object obj) {

		if (obj instanceof KomplexeZahlen) {
			KomplexeZahlen p = (KomplexeZahlen) obj;
			if ((this.Real == p.getReal()) && (this.Imaginaer == p.getImaginaer()))
				return true;
			else
				return false;
		}
		return false;
	}

//Eine Klassenmethode (Zusatz static) zur Multiplikation zweier komplexer Zahlen
	public static KomplexeZahlen getMulti(KomplexeZahlen z1, KomplexeZahlen z2) {
		double re = z1.getReal() * z2.getReal() - z1.getImaginaer() * z2.getImaginaer();
		double im = z1.getImaginaer() * z2.getReal() + z1.getReal() * z2.getImaginaer();
		return new KomplexeZahlen(re, im);

	}

	// Eine Objektmethode zur Multiplikation zweier komplexer Zahlen
	public KomplexeZahlen getMulti1(KomplexeZahlen p) {
		double re = this.getReal() * p.getReal() - this.getImaginaer() * p.getImaginaer();
		double im = this.getImaginaer() * p.getReal() + this.getReal() * p.getImaginaer();
		return new KomplexeZahlen(re, im);

	}

}
