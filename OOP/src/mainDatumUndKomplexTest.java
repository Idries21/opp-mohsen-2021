
public class mainDatumUndKomplexTest {
	public static void main(String[] args) {
		// Aufgabe 3.1
		Datum jan = new Datum(); // Standardkonstruktor
		Datum adam = new Datum(20, 9, 1996); // Konstruktor, der alle Attribute mit Werten versieht.

		// toString()
		System.out.println(jan);
		System.out.println(adam);

		// equals
		if (jan.equals(adam))
			System.out.println("gleich");
		else
			System.out.println("ungleich");

		// aufgabe 3.2
		System.out.println(Datum.getQuartal(jan));
		System.out.println(Datum.getQuartal(adam));

		// aufgabe 3.3
		KomplexeZahlen z1 = new KomplexeZahlen(20, 40);
		KomplexeZahlen z2 = new KomplexeZahlen(10, 30);
		// toString()
		System.out.println(z1);
		System.out.println(z2);

		// equals
		if (z1.equals(z2))
			System.out.println("gleich");
		else
			System.out.println("ungleich");
		KomplexeZahlen z3 = KomplexeZahlen.getMulti(z1, z2); // Eine Klassenmethode (Zusatz static) zur Multiplikation
		// zweier komplexer Zahlen.
		System.out.println(z3);
		// Eine Objektmethode zur Multiplikation zweier komplexer Zahlen
		KomplexeZahlen z4 = z1.getMulti1(z2);
		System.out.println(z4);
	}
}