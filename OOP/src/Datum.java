
public class Datum {
	private int Tag;
	private int Monat;
	private int Jahr;
	private static int Quartal; // statische funktion

	public Datum() { // Standardkonstruktor
		this.Tag = 1;
		this.Monat = 1;
		this.Jahr = 1970;
		this.Quartal = 0;

	}

	public Datum(int Tag, int Monat, int Jahr) { // Konstruktor, der alle Attribute mit Werten versieht.
		this.Tag = Tag;
		this.Monat = Monat;
		this.Jahr = Jahr;

	}

	public void setTag(int Tag) {
		this.Tag = Tag;
	}

	public int getTag() {
		return this.Tag;
	}

	public void setMonat(int Monat) {
		this.Monat = Monat;
	}

	public int getMonat() {
		return this.Monat;
	}

	public void setJahr(int Jahr) {
		this.Jahr = Jahr;
	}

	public int getJahr() {
		return this.Jahr;
	}

	// toString()
	public String toString() {
		return "Geburtsdatum:" + this.Tag + "." + this.Monat + "." + this.Jahr;
	}

	// equals(Object obj)
	public boolean equals(Object obj) {

		if (obj instanceof Datum) {
			Datum p = (Datum) obj;
			if (this.Tag == p.getTag() && this.Monat == p.getMonat() && this.Jahr == p.getJahr())
				return true;
			else
				return false;
		}
		return false;
	}

	// aufgabe 3.2

	public static int getQuartal(Datum Monat) { // statische funktion
		if (Monat.getMonat() <= 3)
			return 1;
		if ((Monat.getMonat() > 3) && (Monat.getMonat() <= 6))
			return 2;
		if ((Monat.getMonat() > 6) && (Monat.getMonat() <= 9))
			return 3;
		else
			return 4;

	}
}
