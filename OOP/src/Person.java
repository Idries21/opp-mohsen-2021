
public class Person {
	private String name;
	private Datum date;
	private static int anzahl;
	private int id;

	public Person(String name, Datum date) {
		this.name = name;
		this.date = date;
		setId();
		anzahl++;
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

	@Override
	public String toString() {
		return " [name=" + name + "," + date + "  Anzahlnum. :" + anzahl + " ID:" +id + "]";
	}

	public static int getAnzahl() {
		return anzahl;
	}
	

	public int getId() {
		return id;
	  }
	public void  setId() {
		this.id = anzahl* 100+ 2004;
	  }

}
