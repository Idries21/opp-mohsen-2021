﻿import java.util.Scanner;

class Arbeitsauftrag_04
{
    public static void main(String[] args)
    {
       Scanner myScanner= new Scanner(System.in);
       double rückgabebetrag;
       double eingezahlterGesamtbetrag;
       int anzahlkarten;
       double zuZahlenderBetrag;
       
       // Anzahl der Tickets 
       anzahlkarten= fahrkartenbestellungErfassen();
       
      // Geldeinwurf
       zuZahlenderBetrag= fahrkartenBezahlen (); 
       zuZahlenderBetrag*=anzahlkarten;
		  eingezahlterGesamtbetrag = (float) 0.0;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("Noch zu zahlen: %.2f Euro%n", (zuZahlenderBetrag- eingezahlterGesamtbetrag));
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   double  eingeworfeneMünze = myScanner.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       } 

       // Fahrscheinausgabe
         fahrkartenAusgeben("\nFahrschein wird ausgegeben");
       
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
      rueckgeldAusgeben(eingezahlterGesamtbetrag ,zuZahlenderBetrag);


    }
    
  public static int  fahrkartenbestellungErfassen() {
	  Scanner myScanner= new Scanner(System.in);
	  System.out.print("Anzahl der Tickets:");
	    int  anzahlkarten = myScanner.nextInt();
	    return anzahlkarten ;
	  
  }
  public static double  fahrkartenBezahlen () {
		 Scanner myScanner= new Scanner(System.in);
		  System.out.print("Zu zahlender Betrag (EURO): ");
		 double zuZahlenderBetrag = (float) myScanner.nextDouble();
		 return zuZahlenderBetrag;
		
  }
  
  public static void fahrkartenAusgeben(String text) {
		 Scanner myScanner= new Scanner(System.in);
		 System.out.println(text);
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
		 
  }
  
  public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag ,double zuZahlenderBetrag) {
	  
    double rückgabebetrag =  eingezahlterGesamtbetrag - zuZahlenderBetrag;
      if(rückgabebetrag > 0.0)
      {
   	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO%n ", rückgabebetrag );
   	   System.out.println("wird in folgenden Münzen ausgezahlt:");

          while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
          {
       	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
          }
          while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
          {
       	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
          }
          while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
          {
       	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
          }
          while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
          {
       	  System.out.println("20 CENT");
	          rückgabebetrag -= 0.2;
          }
          while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
          {
       	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
          }
          while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
          {
       	  System.out.println("5 CENT");
	          rückgabebetrag -= 0.05;
          }
      }

      System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
              "vor Fahrtantritt entwerten zu lassen!\n"+
              "Wir wünschen Ihnen eine gute Fahrt."); 
  
  
  }
  
}





