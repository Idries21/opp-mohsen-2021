
public class Ticket {
	private String bezeichner;
	private double preis;
	private int anzahl;
	
	public Ticket() {
		
	}

	public Ticket(String bezeichner, double preis, int anzahl) {
		this.bezeichner = bezeichner;
		this.preis = preis;
		this.anzahl = anzahl;
	}

	public String getBezeichner() {
		return bezeichner;
	}

	public void setBezeichner(String bezeichner) {
		this.bezeichner = bezeichner;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}

	@Override
	public String toString() {
		return "[" + bezeichner + " " + preis + " " + anzahl + "]";
	}
	
	

}

