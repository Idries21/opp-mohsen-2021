
public abstract class Ausnahmeliste extends Exception {
	private int falschWert;

	public Ausnahmeliste(String message, int falschWert) {
		super(message);
		this.falschWert = falschWert;
	}

	public int getFalschWert() {
		return falschWert;
	}

}
