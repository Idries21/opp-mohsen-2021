
public class Aufgabe6_1 {
	public static void main(String[] args)  {

		try {
			System.out.println(Division(2,0));
		} catch (ArithmeticException ex) {
			System.out.println(ex.getMessage());

		}
	}

	public static double Division(double x, double y) throws ArithmeticException {
		if (y == 0)
			throw new ArithmeticException("error : sie durfen nicht durch null teilen ");
		return x / y;
	}
}
