
public class Datum {
	private int tag;
	private int monat;
	private int jahr;

	public Datum() {
		this.tag = 1;
		this.monat = 1;
		this.jahr = 1970;
	}

	public Datum(int tag, int monat, int jahr) throws AusnahmeTag, AusnahmeMonat, AusnahmeJahr {
		setTag(tag);
		setMonat(monat);
		setJahr(jahr);
	}

	public int getTag() {
		return tag;
	}

	public void setTag(int tag) throws AusnahmeTag {
		if ((tag < 1) || (tag > 31))
			throw new AusnahmeTag("falscher Tag ", tag);
		this.tag = tag;

	}

	public int getMonat() {
		return monat;
	}

	public void setMonat(int monat) throws AusnahmeMonat {
		if ((monat < 1) || (monat > 12))
			throw new AusnahmeMonat("falscher Monat ", monat);
		this.monat = monat;
	}

	public int getJahr() {
		return jahr;
	}

	public void setJahr(int jahr) throws AusnahmeJahr {
		if ((jahr < 1900) || (jahr > 2100))
			throw new AusnahmeJahr("falsches Jahr ", jahr);
		this.jahr = jahr;
	}

	public static int berechneQuartal(Datum d) {
		return d.monat / 3 + 1;
	}

	@Override
	public boolean equals(Object obj) {
		Datum d = (Datum) obj;
		if (this.tag == d.tag && this.monat == d.monat && this.jahr == d.jahr)
			return true;
		return false;
	}

	@Override
	public String toString() {
		return this.tag + "." + this.monat + "." + this.jahr;
	}

}