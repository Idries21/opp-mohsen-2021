package myUtil;

public class MyIntArray implements IIntArray {

	private int[] array;

	public MyIntArray(int index) {
		array = new int[index];
	}
	
//
//	public int[] getArray() {
//		return array;
//	}

	public int get(int index) throws MyIndexException {

		if (index >= this.array.length) {
			throw new MyIndexException(index);
		}
		return array[index];
	}

	public void set(int index, int value) throws MyIndexException {
		if (index >= this.array.length) {
			throw new MyIndexException(index);
		}
		array[index] = value;
	}

	public void increase(int n) {

		int[] tempArray = new int[this.array.length + n];
		for (int i = 0; i < this.array.length; i++)
			tempArray[i] = this.array[i];
		this.array = tempArray;

	}


}
