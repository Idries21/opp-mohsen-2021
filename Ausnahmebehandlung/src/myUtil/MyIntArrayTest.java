package myUtil;

public class MyIntArrayTest {

	public static void main(String[] args) throws MyIndexException {

		IIntArray a1 = new MyIntArray(12);
		a1.increase(2);
		try {
			a1.set(12, 20);
		} catch (MyIndexException ex) {
			System.out.println(ex.getMessage());
			System.out.println(ex.getWrongIndex());
		}
//		System.out.println(a1.get(12));
	}

}
