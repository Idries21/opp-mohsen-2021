import java.util.*;

public class Buchverwaltung {

	static void menue() {
		System.out.println("\n ***** Mitarbeiterverwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) l�schen");
		System.out.println(" 4) Die gro�e ISBN");
		System.out.println(" 5) zeigen");
		System.out.println(" 9) Beenden");
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	public static void main(String[] args) {

//		List<Buch> buchliste = new LinkedList<Buch>();
		List<Buch> buchliste = new ArrayList<Buch>();
		buchliste.add(new Buch(" OOP", "1234","saif"));
		buchliste.add(new Buch(" Java", "1235","Ali"));
		buchliste.add(new Buch(" DB", "1235","Kamal"));
		Scanner myScanner = new Scanner(System.in);

		char wahl;
		String eintrag;

		int index;
		do {
			menue();
			wahl = myScanner.next().charAt(0);
			switch (wahl) {
			case '1':
				System.out.println("Titel :");
				String titel = myScanner.next();
				System.out.println("ISBN: ");
				String isbn = myScanner.next();
				
				System.out.println("Autor :");
				String autor = myScanner.next();
				Buch b = new Buch(titel, isbn,autor);
				buchliste.add(b);
				break;
			case '2':
				System.out.println("Bitte schreiben Sie die ISBN nummer die zu finden ist:");
				String findIsbn = myScanner.next();
				boolean contains = false;
//				for (Buch buch : buchliste ) {
//					if (buch.getIsbn().equals(findIsbn)) {
//						System.out.println(buch.getTitel());
//						contains =true ;
//					}
//				}

				for (Iterator<Buch> it = buchliste.iterator(); it.hasNext();) {
					Buch b2 = it.next();
					if (b2.getIsbn().equals(findIsbn)) {
						System.out.println(b2.getTitel());
						contains = true;
					}
				}
				if (contains == false) {
					System.out.println("Buch nicht gefunden");
				}

				break;
			case '3':
				
				System.out.println("schreiben Sie die ISBN,die Sie l�schen wollen");
				System.out.println("ISBN:");
				 isbn = myScanner.next();
				Buch B =null;
				for (Buch buch : buchliste ) {
					if (buch.getIsbn().equals(isbn)) {
						B= buch;
						break;
					}
				}
				if (B != null)
					buchliste.remove(B);
				else System.out.println("Buch ist nicht vorhanden");
				break;
				
			case '4':
				Collections.sort(buchliste);
				System.out.println(buchliste.get(buchliste.size()-1));
				break;
			case '5':
				System.out.println(buchliste);
				break;
			case '9':
				System.exit(0);
				break;
			default:
				menue();
				wahl = myScanner.next().charAt(0);
			} // switch

		} while (wahl != 9);
		
	}// main

}
