import java.util.Scanner;
public class Arbeitsauftrag5
{
    public static void main(String[] args)
    {
       Scanner myScanner= new Scanner(System.in);
       double r�ckgabebetrag;
       double eingezahlterGesamtbetrag;
       int anzahlkarten;
       double zuZahlenderBetrag;
       
       // Anzahl der Tickets 
       anzahlkarten= fahrkartenbestellungErfassen();
       
      // Geldeinwurf
       zuZahlenderBetrag= fahrkartenBezahlen (); 
       zuZahlenderBetrag*=anzahlkarten;
		  eingezahlterGesamtbetrag = (float) 0.0;
		 
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("Noch zu zahlen: %.2f Euro%n", (zuZahlenderBetrag- eingezahlterGesamtbetrag));
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	   double  eingeworfeneM�nze = myScanner.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
	       } 

       // Fahrscheinausgabe
         fahrkartenAusgeben("\nFahrschein wird ausgegeben");
       
       // R�ckgeldberechnung und -Ausgabe
      rueckgeldAusgeben(eingezahlterGesamtbetrag ,zuZahlenderBetrag);


    }
    
    
  public static int  fahrkartenbestellungErfassen() {
	  Scanner myScanner= new Scanner(System.in);
	  System.out.print("Anzahl der Tickets:");
	    int  anzahlkarten = myScanner.nextInt();
	    while(!((anzahlkarten >=1)&&( anzahlkarten<=10)) ) {
	    	System.out.println("fahlsche Eingabe: Sie k�nnen eine Anzahl zw. 1 und 10 auswahlen");
	  	    System.out.print("Anzahl der Tickets:");
		     anzahlkarten = myScanner.nextInt();
	    }
	    return anzahlkarten ;
	    }
	  
 
  public static double  fahrkartenBezahlen () {
		 Scanner myScanner= new Scanner(System.in);
		  System.out.print("Zu zahlender Betrag (EURO): ");
		 double zuZahlenderBetrag = (float) myScanner.nextDouble();
		  if ( zuZahlenderBetrag >=0) {
		    	
			  return zuZahlenderBetrag;
			    }
			    else {
			    	while(zuZahlenderBetrag<0) {
			    	System.out.println("fahlsche Eingabe: Sie k�nnen Keine negative Eingabe eingeben");
					  System.out.print("Zu zahlender Betrag (EURO): ");
					   zuZahlenderBetrag = (float) myScanner.nextDouble();
			    	}
					   return zuZahlenderBetrag;
			    }
		
		
  }
  
  public static void fahrkartenAusgeben(String text) {
		 Scanner myScanner= new Scanner(System.in);
		 System.out.println(text);
		 warte(1);
		 
  }
  
  public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag ,double zuZahlenderBetrag) {
	  
    double r�ckgabebetrag =  eingezahlterGesamtbetrag - zuZahlenderBetrag;
      if(r�ckgabebetrag > 0.0)
      {
   	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO%n ", r�ckgabebetrag );
   	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

          while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
          {
       	  System.out.println("2 EURO");
	          r�ckgabebetrag -= 2.0;
          }
          while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
          {
        	  muenzeAusgeben(1, "EURO");
	          r�ckgabebetrag -= 1.0;
          }
          while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
          {
       	  System.out.println("50 CENT");
	          r�ckgabebetrag -= 0.5;
          }
          while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
          {
       	  System.out.println("20 CENT");
	          r�ckgabebetrag -= 0.2;
          }
          while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
          {
       	  System.out.println("10 CENT");
	          r�ckgabebetrag -= 0.1;
          }
          while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
          {
       	  System.out.println("5 CENT");
	          r�ckgabebetrag -= 0.05;
          }
      }

      System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
              "vor Fahrtantritt entwerten zu lassen!\n"+
              "Wir w�nschen Ihnen eine gute Fahrt."); 
  
  
  }
  
  public static void warte(int millisekunde) {
      for (int i = 0; i < 8; i++)
      {
         System.out.print("=");
         try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
      }
      System.out.println("\n\n");
	  
	  
  }
  
  public static void muenzeAusgeben(int betrag, String einheit) {
	  System.out.printf("%d %s%n",betrag,einheit);
      
  }

  
}





