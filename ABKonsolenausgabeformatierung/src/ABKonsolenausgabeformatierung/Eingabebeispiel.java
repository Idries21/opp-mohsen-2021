// class homework for Eingabebeispiel 
package ABKonsolenausgabeformatierung;

import java.util.Scanner;
public class Eingabebeispiel {
	
	public static void main (String[]args ) {
		Scanner myScanner= new Scanner(System.in);
		
		System.out.println(" Geben Sie Ihren Vorname bitte ");
		String vorstr = myScanner.next();
		
		System.out.println(" Geben Sie Ihren Nachname bitte ");
		String nachstr = myScanner.next();
		
		System.out.println(" Geben Sie Ihren Alter bitte ");
		int Alt = myScanner.nextInt();
		
		System.out.printf("Vorname :%-10s%n",vorstr);
		System.out.printf("Nachname :%-10s%n",nachstr);
		System.out.printf("Alter :      %-10d%n",Alt);
		
		
		
	}

}
